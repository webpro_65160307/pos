import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import '@mdi/font/css/materialdesignicons.css'
import router from './router'
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const vuetify = createVuetify({
    icons: {
      defaultSet: 'mdi',
    },
    components,
    directives,
  })
const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(vuetify)
app.mount('#app')
