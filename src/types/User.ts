type Gender = 'male' | 'female' | 'others'
type Role = 'admin' | 'user' | 'others'

type User = {
  id: number
  email: string
  password: string
  fullname: string
  gender: Gender 
  roles: Role[] 
}

export type { Gender, Role, User }